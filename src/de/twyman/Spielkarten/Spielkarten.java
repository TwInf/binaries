/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.Spielkarten;

import static de.twyman.Spielkarten.Spielkarten.Farben.JOKER;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 * Eine Spielkarte.
 * Die Karten Nummern gehen von 2 bis 14 (Ass)
 * Die Karten Farben sind durch die Aufzählung Farben
 * genannt, dort ist eine Joker auch definiert.
 * Bildmengen definitiert die mögliche eingebauet
 * Sammelung von Kartenbilde.  Nicht alle Mengen
 * haben eine Joker, falls keine vorhanden ist
 * kommt kein Bild bei der Abruf.
 * @author D. Twyman
 */
public class Spielkarten implements Comparable<Spielkarten> 
                                   ,Comparator<Spielkarten> {
    protected final String[] Buchstabe1 = {"02","03","04",
                                         "05","06",
                                         "07","08","09",
                                         "10", //ten
                                         "11", //jack-Bube
                                         "12", //queen-Königin
                                         "13", //king-König
                                         "01"  //ass
                                         };
    protected boolean bVorwarts = false;
    protected int iKartenzahl; //2 bis 10, Bube, Königin, König, Ass
                             //2-9, ten, Jack, Queen, King, Ace
    /**
     * Die mögliche Farben von Spielkarten.
     */
    public enum Farben {
                    /**
                     * ist für Karten von Type Pik, bzw. Spades
                     */
                    PIK, 
                    /**
                     * ist für Karten von Type Herz, bzw Hearts.
                     */
                    HERZ, 
                    /**
                     * ist für Karten von Type Karo, bzw Diamonds.
                     */
                    KARO, 
                    /**
                     * ist für Karten von Type Kreuz, bzw Clubs.
                     */
                    KREUZ, 
                    /**
                     * ist für Karten von Type Joker, diese Karten
                     * haben keine relevante 'Wert'.
                     */
                    JOKER };
    /**
     * Die mögliche Optionen von eingebauet
     * Bilder für die Karten. 
     * Die Bilder sind alle unter GPL 2
     * abrufbar, der Defaultset stammt von
     * http://www.waste.org/~oxymoron/cards/COPYING
     * Die restliche Bildmenge sind aus der
     * PySol cardsets.
     */
    public enum Bildmengen
                {
                /**
                 * ist die Default, eine einfache Warkwick-artige Bild.
                 * Ein Joker ist vorhanden.
                 */
                DEFAULT, 
                /**
                 * ist eine 1890 Version der Grimaund Karten.
                 * Kein Joker ist vorhanden.
                 */
                GRIMAUD, 
                /**
                 * ist ein historische Hamburg Kartenbild.
                 * Kein Joker ist vorhanden.
                 */
                HAMBURG, 
                /**
                 * ist eine historische inidische Kartenbild.
                 * Das Kartenwert ist klar zu lesen.
                 * Kein Joker ist vorhanden.
                 */
                INDIA, 
                /**
                 * ist etwas bunte version der Warwickset als das default.
                 * Ein Joker ist vorhanden.
                 */
                WARWICK };
    protected Farben Farbe;
    protected String cardset = "default";

    /**
     * SpielKarte erstellen, 
     * zufällig aus alle mögliche Karten.
     * Hier eine Deck von Poker-Karten
     * ergibt die Wahrscheinlichkeiten
     * von Einzelnkarten.
     * Der Karte ist immer gedeckt.
     */
    public Spielkarten(){
        Random rnd = new Random();
        // Zufällige Farbe für die Spielkarte
        switch (rnd.nextInt(3)) {
            case 0:
                Farbe = Farben.PIK;
                break;
            case 1:
                Farbe = Farben.HERZ;
                break;
            case 2:
                Farbe = Farben.KARO;
                break;
            case 3:
                Farbe = Farben.KREUZ;       
                break;
        }
        // Zufällige Wert zwischen 2 und 14, inklusiv.
        iKartenzahl = rnd.nextInt(12)+2; 
    }
    
    /**
     * Bestimmte SpielKarte erstellen.
     * Ungültige Werte werden in gültige Werte umgewandelt.
     * Wert ist 2 bis 10, Bube (11), 
     * Königin (12), König (13), Ass (14).
     * Der Karte ist gedeckt.
     * @param fFarbe - die Farbe der neue Karte
     * @param iZahl - das Wert der neue Karte, wird auf 2 bis 14 eingeschränkt.
     */
    public Spielkarten(Farben fFarbe
                      ,int iZahl){
        Farbe = fFarbe;
        iKartenzahl = iZahl; 
        if (iKartenzahl > 14) {
            iKartenzahl = 14;
        }
        if (iKartenzahl < 2) {
            iKartenzahl = 2;
        }
        if (Farben.JOKER == fFarbe) {
            iKartenzahl = 2; 
        }
    }
    
    /**
     * Implementiert die java.lang.Comparable Methode. <br>
     * -1 falls diese Objekt ist kleiner als b <br>
     * 0 falls diese Objekt ist gleich b <br>
     * 1 falls diese Objekt ist großer als b
     * @param b
     */
    @Override
    public int compareTo(Spielkarten b) {
        int iMeinFarbe = farbeAlsInt(Farbe);
        int iFarbeB    = farbeAlsInt(b.getSpielfarbe());
        if(iMeinFarbe>iFarbeB) {
            return 1; // Mein Farbe ist großer
        }
        else if (iMeinFarbe<iFarbeB) {
            return -1; //Mein Farbe ist kleiner
        }
        else {  // die Farben sind gleich
            if (iKartenzahl>b.getWert()) {
                return 1;  // Mein Wert ist großer
            }
            else if (iKartenzahl<b.getWert()) {
                return -1;  // Mein Wert ist kleiner
            }
            else {  // Die Farben und Werte sind beide Gleich
                return 0;
            }
        }
    }
    
    /**
     * Implementiert die java.lang.Comparator Methode. <br>
     * -1 falls A ist kleiner als B <br>
     *  0 falls A ist gleich B <br>
     *  1 falls A ist großer als B.
     * @param A
     * @param B
     */
    @Override
    public int compare(Spielkarten A, Spielkarten B) {
        int iFarbeA = farbeAlsInt(A.getSpielfarbe());
        int iFarbeB = farbeAlsInt(B.getSpielfarbe());
        if(iFarbeA>iFarbeB) {
            return 1; // Farbe A ist großer
        }
        else if (iFarbeA<iFarbeB) {
            return -1; // Farbe A ist kleiner
        }
        else {  // die Farben sind gleich
            if (A.getWert()>B.getWert()) {
                return 1;  // Wert A ist großer
            }
            else if (A.getWert()<B.getWert()) {
                return -1;  // Wert A ist kleiner
            }
            else {  // Die Farben und Werte sind beide Gleich
                return 0;
            }
        }
    }
    
    /**
     * Die Karte wird gedreht.
     */
    public void drehen(){
        bVorwarts = !bVorwarts;
    }
    
    /**
     * Die Karte wird gedeckt.
     */
    public void decken() {
        bVorwarts = false;
    }
            
    /**
     * Die Karte wird aufgedeckt.
     */
    public void aufdecken() {
        bVorwarts = true;
    }
    
    /*
    Eine Werkzeug-funktion, 
    um die Vergleich zu vereinfachen
    */
    final protected int farbeAlsInt(Farben fFarbe){
        int retVal;
        switch (fFarbe) {
            case PIK:
                retVal=0;  
                break;
            case HERZ:
                retVal=1;
                break;
            case KARO:
                retVal=2;
                break;
            case KREUZ:
                retVal=3;
                break;   
            case JOKER:
            default:
                retVal=4;
                break;
        }
        
        return retVal;
    }
    
    /*
    Eine Werkzeug-funktion, 
    um die Dateiname zu erzeugen
    */
    protected String farbeAlsBuchstabe(){
        String retVal = "";
        switch (Farbe) {
            case PIK:
                retVal="s";  //Spades
                break;
            case HERZ:
                retVal="h";  //hearts
                break;
            case KARO:
                retVal="d";  // diamonds
                break;
            case KREUZ:
                retVal="c";  // clubs
                break;       
        }
        
        return retVal;
    }
    
    /**
     * Die Bilder für die Karten werden ausgewählt.
     * @param einBildmenge - welche Bildmenge soll benutzt werden.
     */
    public void setBildMenge ( Bildmengen einBildmenge) {      
        switch (einBildmenge) {
            case GRIMAUD:
                cardset = "1890-grimaud";
                break;
            case HAMBURG:
                cardset = "hamburg-a";
                break;
            case INDIA:
                cardset = "india-pantheon";
                break;
            case WARWICK:
                cardset = "warwick";
                break;
            default:
                cardset = "default";
                break;       
        }
    }
            
    /**
     * Ein Bild von diese Karte als icon.
     * Falls die Karte gedeckt ist wird eine 'Rückseite'
     * Abbildung zurückgegeben, sonst eine passende
     * Bild für diese Farbe und Wert.
     * @return ImageIcon - das Abbildung diese Karte 
     */
    public ImageIcon getIcon(){
        String Filename;
        String FilenameStam = "cardset-"+ this.cardset+"/";
        if (!bVorwarts) {
            Filename = FilenameStam.concat("back01.gif");
        } else if (Farben.JOKER == Farbe) {
            Filename = FilenameStam.concat("01z.gif");
        } else {
            Filename =  FilenameStam.concat(Buchstabe1[iKartenzahl-2]
                        +this.farbeAlsBuchstabe()
                        +".gif");
        }
        ImageIcon icon;
        URL einURL = this.getClass().getResource(Filename);
        if (einURL!=null)
            icon = new ImageIcon(einURL);
        else 
            icon = new ImageIcon();
        return icon;
    } 
    
    /**
     * Gibt die Spielkartenfarbe von diese Karte zurück.
     * @return Farbe - das Farbe von diese Karte.
     */
    public Farben getSpielfarbe() {
        return Farbe;
    }
            
    /**
     * Gibt die Wert von diese Karte zurück.
     * @return Wert - das Wert von diese Karte, 2 (eine 2) bis 14 (ein Ass).
     */
    public int getWert() {
        return iKartenzahl;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Arrays.deepHashCode(this.Buchstabe1);
        hash = 29 * hash + this.iKartenzahl;
        hash = 29 * hash + Objects.hashCode(this.Farbe);
        hash = 29 * hash + Objects.hashCode(this.cardset);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Spielkarten other = (Spielkarten) obj;
        if (!Objects.equals(this.cardset, other.cardset)) {
            return false;
        }
        if (this.Farbe != other.Farbe) {
            return false;
        }
        if (JOKER == this.Farbe)
            return true;
        if (this.iKartenzahl != other.iKartenzahl) {
            return false;
        }
        return true;
    }
    
    
}
