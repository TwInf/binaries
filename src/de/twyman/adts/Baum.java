/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;

/**
 * Eine Baum als Abstraktdatentyp nach
 * der Vorlage von der Landesschulbehörde
 * in Niedersachsen für das Abi 2015
 * Nur für Erhöhten Anforderung nötig.
 * @author Twyman
 * @param <Inhalt> - Der Datentyp der gespeichert werden soll.
 */
public class Baum<Inhalt> {
    // der Inhalt von eine BaumNode
    private class BaumElement
    {
        Inhalt NodeInhalt = null;
        Baum linksUnterbaum = null;
        Baum rechtsUnterbaum = null;
    }
    private BaumElement Würzel = new BaumElement();
    
    /** 
     * Ein Baum wird erzeugt.
     */
    public Baum() {
    }

    /** Ein Baum wird erzeugt. Die Wurzel 
     * erhält den übergebenen Inhalt als Wert. 
     * @param inhalt
     */
    public Baum(Inhalt inhalt) {
        Würzel.NodeInhalt = inhalt;
    }
    
    /**
     * Die Anfrage liefert den Wert wahrBaum
     * wenn der Baum keine Nachfolger hat, 
     * sonst liefertsie den Wert falsch. 
     * @return boolean
     */
    public boolean istBlatt() {
        return ((Würzel.linksUnterbaum  == null) &&
                (Würzel.rechtsUnterbaum == null)   );
    }

    /**
     * Die Anfrage liefert den Wert wahr, 
     * Baum wenn der Baum leer ist, sonst liefert 
     * sie den Wert falsch. 
     * @return boolean - wahr, falls der Baum leer ist, sonst falsch.
     * eine Leere Baum ist eine Baum der kein Daten enthält und
     * selbst ein Blatt ist.
     */
    public boolean istLeer() {
        return (istBlatt() && null==Würzel.NodeInhalt);
    }
    
    /**
     * Der übergebene Baum wird als linker 
     * Teilbaum gesetzt. 
     * @param b
     */
    public void linkenTeilbaumSetzen(Baum b) {
        Würzel.linksUnterbaum = b;
    }

    /**
     * Die Operation gibt den linken Teilbaum zurück. 
     * Existiert kein linker Nachfolger, 
     * so ist das Ergebnis null. 
     * @return Baum
     */
    public Baum linkerTeilbaum() {
        return Würzel.linksUnterbaum;
    }

    /**
     * Der übergebene Baum wird als rechter 
     * Teilbaum gesetzt.
     * @param b
     */
    public void rechtenTeilbaumSetzen(Baum b) {
        Würzel.rechtsUnterbaum = b;
    }
    
    /**
     * Die Operation gibt den rechten Teilbaum zurück. 
     * Existiert kein rechter Nachfolger, 
     * so ist das Ergebnis null. 
     * @return Baum
     */
    public Baum rechterTeilbaum() {
        return Würzel.rechtsUnterbaum;
    }
    
    /**
     * Die Operation gibt den Inhaltswert 
     * der Wurzel des aktuellen Baumes zurück. 
     * @return Inhalt
     */
    public Inhalt inhaltGeben() {
        return (Inhalt) Würzel.NodeInhalt;
    }

    /**
     * Die Operation setzt den Inhaltswert der 
     * Wurzel des aktuellen Baumes.     
     * @param inhalt
     */
    public void inhaltSetzen(Inhalt inhalt)  {
        Würzel.NodeInhalt = inhalt;
    }
    
    //für eine EqualsMethode sollte hier alle
    // Elementen untersucht die jeweils vergliechen.
}
