/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Eine Liste als Abstraktdatentyp nach
 * der Vorlage von der Landesschulbehörde
 * in Niedersachsen für das Abi 2015
 * Für grundlegende und erhöhten Anforderung.
 * @author Twyman
 * @param <Inhalt> - Der Datentyp der gespeichert werden soll.
 */
public class Liste<Inhalt> {
    // Der Java Util Implementierung von eine Liste
    protected LinkedList javaLinkedListe;
    //der Aktuelle Element in der Liste
    protected Integer aktuellenElement = null;
    
    /**
     * Der Inhalt des aktuellen Listenelements 
     * wird zurückgegeben.
     * @return Inhalt - das aktuelle Element in der Liste.
     */
    public Inhalt inhaltGeben() {
        if (istLeer()) {
            return null;
        }
        else
            return (Inhalt) javaLinkedListe.get(aktuellenElement);
    }
    
    /**
     * Wenn die Liste kein Element enthält, 
     * wird der Wert wahr zurückgegeben, 
     * sonst der Wert falsch.
     * @return boolean - wahr, falls die Liste leer ist, sonst falsch.
     */
    public boolean istLeer() {
        return (javaLinkedListe.isEmpty());
    }

    /**
     * Ein neues Listenelement mit dem angegebenen 
     * Inhalt wird angelegt und hinter der aktuellen 
     * Position in die Liste eingefügt, alle 
     * weiteren Elemente werden nach hinten verschoben. 
     * Der interne Positionszeiger steht auf dem 
     * neu eingefügten Element. 
     * @param inhalt - das neue Element für die Liste
     */
    public void einfuegen(Inhalt inhalt) {
        if (istLeer()) {
            aktuellenElement = -1;
        }
        javaLinkedListe.add(aktuellenElement+1, inhalt);
        aktuellenElement = aktuellenElement+1;
    };
    
    /**    
     * Ein neues Listenelement mit dem angegebenen 
     * Inhalt wird angelegt und an der angegebenen 
     * Position in die Liste eingefügt. Das vorher 
     * an dieser Position befindliche Element und alle 
     * weiteren Elemente werden nach hinten verschoben. 
     * Der interne Positionszeiger steht auf dem neu 
     * eingefügten Element. Falls die angegebene Position 
     * nicht existiert, hat die Operation keine Wirkung. 
     * @param position - die gewünschte Position. Die muss gültige für dies Liste sein.
     * Die Positionen werden von 0 bis 'Länge der Liste'-1 gezählt.
     * @param inhalt - die neue Inhalt.
     */
    public void einfuegen(Integer position, Inhalt inhalt) {
        if (null!=position) {
            if (  (position <= javaLinkedListe.size())
                &&(position >=0) ) {
                javaLinkedListe.add((int)position, inhalt);
                aktuellenElement = position;
            }
        }
    };

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Liste<?> other = (Liste<?>) obj;
        //
        if (!(this.javaLinkedListe.equals(other.javaLinkedListe))) {
            return false;
        }
        if (this.aktuellenElement != other.aktuellenElement) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.javaLinkedListe);
        hash = 59 * hash + this.aktuellenElement;
        return hash;
    }

    /**
     * Die Anzahl der Elemente in der Liste 
     * wird zurückgegeben. 
     * @return Integer - Anzahl von Elemente in der Liste.
     */    
    public Integer laengeGeben() {
        return javaLinkedListe.size();
    }
    
    /**
     * Eine leere Liste wird angelegt. 
     * Der interne Positionszeiger, der das aktuelle 
     * Element markiert, wird auf null gesetzt
     */
    public Liste() {
        javaLinkedListe = new LinkedList<>();
    }

    /**
     * Das aktuelle Listenelement wird gelöscht. 
     * Der interne Positionszeiger steht anschließend 
     * auf dem Nachfolger des gelöschten Elements. 
     * Falls kein Nachfolger existiert, zeigt er auf 
     * den Vorgänger. 
     */
    public void loeschen() {
        javaLinkedListe.remove((int)aktuellenElement);
        if (aktuellenElement >= javaLinkedListe.size())
            aktuellenElement=aktuellenElement-1;
        if (aktuellenElement<0)
            aktuellenElement = null;       
    }

    /**
     * Der Wert des internen Positionszeigers wird 
     * zurückgegeben. 
     * @return Integer - der jetzige Position, darf auch null sein.
     */
    public Integer positionGeben() {
        return aktuellenElement;
    }
  
    /**
     * Der interne Positionszeiger wird auf den 
     * angegebenen Wert gesetzt. Falls die angegebene 
     * Position nicht existiert, wird der 
     * Positionszeiger nicht verändert. 
     * @param position - das neue Position, falls die gültig für unsere Liste ist.
     * Die Positionen werden von 0 bis 'Länge der Liste'-1 gezählt.
     */
    public void positionSetzen(Integer position) {
        if (null!=position) {
            if (   (position >=0) 
                && (position<javaLinkedListe.size())) {
                aktuellenElement = position;
            }
        }
    }
}
