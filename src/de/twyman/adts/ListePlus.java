/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;
import java.util.Objects;

/**
 * Eine erweiterung der Liste als Abstraktdatentyp nach
 * der Vorlage von der Landesschulbehörde
 * in Niedersachsen für das Abi 2015
 * Kopierien, Löschen und vergleichen die
 * Listen ist auch möglich
 * @author Twyman
 * @param <Inhalt>
 */
public class ListePlus<Inhalt> extends Liste<Inhalt> {

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof ListePlus) {
            final ListePlus<?> other = (ListePlus<?>) obj;
            //
            if (!(this.javaLinkedListe.equals(other.javaLinkedListe))) {
                return false;
            }
            return (this.aktuellenElement.equals(other.aktuellenElement));
        }
        if (!(obj instanceof Liste))
            return false;
        final Liste<?> other = (Liste<?>) obj;
        //
        return super.equals(other);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.aktuellenElement;
        hash = 29 * hash + Objects.hashCode(this.javaLinkedListe);
        return hash;
    }
    
    /**
     * Fügt eine Liste diese hinzu.
     * Die Elemente sind aber
     * nicht kopiert, nur die Verweise sind dupliziert.
     * @param Hinzu
     */
    public void hinzufuegen(Liste<Inhalt> Hinzu) {
        if (Hinzu==null)
            return;
        
        Integer tmpPos1 = Hinzu.positionGeben();
        Integer tmpPos2 = this.positionGeben();
        
        for(int i=0; i<Hinzu.laengeGeben(); i++) {
            Hinzu.positionSetzen(i);
            Inhalt tmp = Hinzu.inhaltGeben();
            this.einfuegen(this.laengeGeben()-1,tmp);
        }
        
        Hinzu.positionSetzen(tmpPos1);
        this.positionSetzen(tmpPos2);
    }
    
    /**
     * Inhalt an eine bestimmte Position zurückgeben.
     * Falls das Position nicht gültig ist, dann wird
     * null zurückgegeben. <br>
     * Der interne Positionzeige wird hier nicht beeinflusst.
     * @param j
     * @return Inhalt
     */
    public Inhalt inhaltGeben(Integer j) {
        if ((j<0) || ( j>=this.laengeGeben() ) )
                return null;
        
        Integer altPos = this.positionGeben();
        this.positionSetzen(j);
        
        Inhalt gewuenschteInhalt = this.inhaltGeben();
        this.positionSetzen(altPos);
        return gewuenschteInhalt;
    }

    /**
     * Kopiert diese Liste, die Elemente sind aber
     * nicht kopiert, nur die Verweise sind dupliziert.
     * @return Verweis auf eine Kopie diese Liste
     */
    public ListePlus<Inhalt> kopie() {
        ListePlus<Inhalt> neueListe = new ListePlus<Inhalt>();

        for(int i=0;i<this.laengeGeben();i=i+1) {
            this.positionSetzen(i);
            neueListe.einfuegen(this.inhaltGeben());
        }
        return neueListe;
    }

    /**
     * Lösche eine bestimmte Elemente.
     * Die Elemente sind aber
     * nicht kopiert, nur die Verweise sind dupliziert.
     * @param j Position des Element der gelöscht werden soll.
     */
    public void loeschen(int j) {
        if ((j<0) || ( j>=this.laengeGeben() ) )
                return;
        
        Integer altPos = this.positionGeben();
        this.positionSetzen(j);
        this.loeschen();
        if (altPos>j)
            altPos=altPos-1;
        
        this.positionSetzen(altPos);
    }
    
    /**
     * Löscht die gesamte Liste.
     */
    public void vernichten() {
        while (!this.istLeer()) {
            this.loeschen();
        }
    }
}
