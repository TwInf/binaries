/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;

import java.util.Queue;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Eine Schlange als Abstraktdatentyp nach
 * der Vorlage von der Landesschulbehörde
 * in Niedersachsen für das Abi 2015
 * Für grundlegende und erhöhten Anforderung.
 * @author Twyman
 * @param <Inhalt> - Der Datentyp der gespeichert werden soll.
 */
public class Schlange<Inhalt> {
    // Der Java Util Implementierung von eine Liste
    protected Queue<Inhalt> javaSchlange;

    /**
     * Ein neues Element mit dem angegebenen 
     * Inhalt wird angelegt und am Ende an die 
     * Schlange angehängt. 
     * @param inhalt - das neue Element für die Schlange.
     */
    public void anhaengen(Inhalt inhalt) {
        javaSchlange.add(inhalt);
    }

    /** 
     * Der Inhalt des ersten Elements wird 
     * zurückgegeben und das Element wird entfernt. 
     * @return Inhalt - das ehrmalige erste Element in der Schlange.
     */
    public Inhalt entnehmen() {
        return (Inhalt) javaSchlange.remove();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Schlange<?> other = (Schlange<?>) obj;
        //
        if (!(this.javaSchlange.equals(other.javaSchlange))) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.javaSchlange);
        return hash;
    }

    /**
     * Wenn die Schlange kein Element enthält, 
     * wird der Wert wahr zurückgegeben, 
     * sonst der Wert falsch. 
     * @return boolean - wahr, falls der Schlange leer ist, sonst falsch.
     */
    public boolean istLeer() {
        return javaSchlange.isEmpty();
    }
    
    /**
     * Der Inhalt des ersten Elements 
     * der Schlange wird zurückgegeben, 
     * das Element aber nicht entfernt. 
     * @return Inhalt - das erste Element in die Schlange.
    */
    public Inhalt inhaltGeben() {
        return (Inhalt) javaSchlange.peek();
    } 

    /** 
     * Eine leere Schlange wird angelegt. 
     */
    public Schlange() {
        javaSchlange=new LinkedList<>();
    }
}
