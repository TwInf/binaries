/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;

import java.util.Objects;

/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Eine erweiterung der Schlange als Abstraktdatentyp nach
 * der Vorlage von der Landesschulbehörde
 * in Niedersachsen für das Abi 2015
 * Kopierien, Loeschen, LaengeGeben und vergleichen die
 * Schlange ist auch möglich
 * @author Twyman
 * @param <Inhalt>
 */
public class SchlangePlus<Inhalt> extends Schlange<Inhalt> {
    Integer Laenge; // Laenge beobachten

    /** 
     * Eine leere Schlange wird angelegt. 
     */
    public SchlangePlus() {
        super();
        Laenge = 0;
    }
    
    /**
     * Ein neues Element mit dem angegebenen 
     * Inhalt wird angelegt und am Ende an die 
     * Schlange angehängt. 
     * @param inhalt - das neue Element für die Schlange.
     */
    @Override
    public void anhaengen(Inhalt inhalt) {
        super.anhaengen(inhalt);
        Laenge++;
    }
    
    /** 
     * Der Inhalt des ersten Elements wird 
     * zurückgegeben und das Element wird entfernt. 
     * @return Inhalt - das ehrmalige erste Element in der Schlange.
     */
    @Override
    public Inhalt entnehmen() {
        Inhalt tmp = super.entnehmen();
        Laenge--;
        return tmp;
    }
    
    /**
     * Die Anzahl der Elemente in der Schlange
     * wird zurückgegeben. 
     * @return Integer - Anzahl von Elemente in der Liste.
     */    
    public Integer laengeGeben() {
        return Laenge;
    }
    
    /**
     * Kopiert diese Schlange, die Elemente sind aber
     * nicht kopiert, nur die Verweise sind dupliziert.
     * @return Verweis auf eine Kopie diese Liste
     */
    public SchlangePlus<Inhalt> kopie() {
        SchlangePlus<Inhalt> tmpSchlange  = new SchlangePlus<Inhalt>();
        SchlangePlus<Inhalt> neueSchlange = new SchlangePlus<Inhalt>();

        while(!this.istLeer()) {
            tmpSchlange.anhaengen(this.entnehmen());
        }
        while(!tmpSchlange.istLeer()) {
            neueSchlange.anhaengen(tmpSchlange.inhaltGeben());
            this.anhaengen(tmpSchlange.entnehmen());
        }
        return neueSchlange;
    }
    
    /**
     * Fügt eine Schlange diese hinzu.
     * Die Elemente sind aber
     * nicht kopiert, nur die Verweise sind dupliziert.
     * @param Hinzu
     */
    public void hinzufuegen(Schlange<Inhalt> Hinzu) {
        if (Hinzu==null)
            return;
        
        Schlange<Inhalt> stplZwischen = new Schlange<>();
        while(!Hinzu.istLeer()) {
            stplZwischen.anhaengen(Hinzu.entnehmen());
        }
        
        while(!stplZwischen.istLeer()) {
            Hinzu.anhaengen(stplZwischen.inhaltGeben());
            this.anhaengen(stplZwischen.entnehmen());
        }   
    }
    
    /**
     * Löscht die gesamte Schlange.
     */
    public void vernichten() {
        while(!this.istLeer()) {
            this.entnehmen();
        }        
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof SchlangePlus) {
            final SchlangePlus<?> other = (SchlangePlus<?>) obj;
            //
            if (!(this.javaSchlange.equals(other.javaSchlange))) {
                return false;
            }
            return true;
        }
        if (!(obj instanceof Schlange))
            return false;
        final Schlange<?> other = (Schlange<?>) obj;
        //
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        int hash = 9;
        hash = 29 * hash + this.Laenge;
        hash = 29 * hash + Objects.hashCode(this.javaSchlange);
        return hash;
    }
}
