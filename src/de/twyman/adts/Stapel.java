/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;

import java.util.EmptyStackException;
import java.util.Objects;
import java.util.Stack;

/**
 * Eine Stapel als Abstraktdatentyp nach
 * der Vorlage von der Landesschulbehörde
 * in Niedersachsen für das Abi 2015
 * Für grundlegende und erhöhten Anforderung.
 * @author Twyman
 * @param <Inhalt> - Der Datentyp der gespeichert werden soll.
 */
public class Stapel<Inhalt> {
    // Der Java Util Implementierung von eine Stapel
    protected Stack javaStapel;
    
    /**
     * Ein neues Element mit dem angegebenen 
     * Inhalt wird auf den Stapel gelegt.
     * @param inhalt - das neue Element für der Stapel
     */
    public void ablegen(Inhalt inhalt) {
        javaStapel.push(inhalt);
    };
    
    /** 
     * Der Inhalt des obersten Elements wird 
     * zurückgegebenund das Element wird entfernt.  
     * @return Inhalt - das ehmalige oberste Element des Stapels.
     */
    public Inhalt entnehmen() {
        return (Inhalt) javaStapel.pop();
    };
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stapel<?> other = (Stapel<?>) obj;
        //
        if (!(this.javaStapel.equals(other.javaStapel))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.javaStapel);
        return hash;
    }
    
    /**
     * Der Inhalt des obersten Elements des Stapels 
     * wird zurückgegeben, das Element aber nicht entfernt. 
     * @return Inhalt - Das Element oben auf der Stapel.
     */
    public Inhalt inhaltGeben() {
        Inhalt retVal=null;
        try {
            retVal = (Inhalt) javaStapel.peek();
        }
        catch (EmptyStackException ex) {
            retVal=null;
        }
        return retVal;
    }
    
    /**
     * Wenn der Stapel kein Element enthält, 
     * wird der Wert wahr zurückgegeben, 
     * sonst der Wert falsch.
     * @return boolean - wahr, falls der Stapel leer ist, sonst falsch.
     */
    public boolean istLeer() {
        return (javaStapel.isEmpty());
    }

    /**
     * Ein leerer Stapel wird angelegt.
     */
    public Stapel() {
        javaStapel = new Stack<>();
    }

}
