/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts;

import java.util.Objects;

/**
 * Eine erweiterung der Stapel als Abstraktdatentyp nach
 * der Vorlage von der Landesschulbehörde
 * in Niedersachsen für das Abi 2015
 * Kopierien, Löschen, LängeGeben und vergleichen die
 * Stapeln ist auch möglich
 * @author Twyman
 * @param <Inhalt>
 */
public class StapelPlus<Inhalt> extends Stapel<Inhalt> {
    Integer Laenge; // Laenge beobachten

    /**
     * Ein neues Element mit dem angegebenen 
     * Inhalt wird auf den Stapel gelegt.
     * @param inhalt - das neue Element für der Stapel
     */
    @Override
    public void ablegen(Inhalt inhalt) {
        super.ablegen(inhalt);
        Laenge++;
    }
    
    /** 
     * Der Inhalt des obersten Elements wird 
     * zurückgegebenund das Element wird entfernt.  
     * @return Inhalt - das ehmalige oberste Element des Stapels.
     */
    @Override
    public Inhalt entnehmen() {
        Inhalt tmp = super.entnehmen();
        Laenge--;
        return tmp;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof StapelPlus) {
            final StapelPlus<?> other = (StapelPlus<?>) obj;
            //
            if (!(this.javaStapel.equals(other.javaStapel))) {
                return false;
            }
            return true;
        }
        if (!(obj instanceof Stapel))
            return false;
        final Stapel<?> other = (Stapel<?>) obj;
        //
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        int hash = 9;
        hash = 29 * hash + this.Laenge;
        hash = 29 * hash + Objects.hashCode(this.javaStapel);
        return hash;
    }
    
    /**
     * Fügt eine Stapel diese hinzu.
     * Die Elemente sind aber
     * nicht kopiert, nur die Verweise sind dupliziert.
     * @param Hinzu
     */
    public void hinzufuegen(Stapel<Inhalt> Hinzu) {
        if (Hinzu==null)
            return;
        
        Stapel<Inhalt> stplZwischen = new Stapel<>();
        while(!Hinzu.istLeer()) {
            stplZwischen.ablegen(Hinzu.entnehmen());
        }
        
        while(!stplZwischen.istLeer()) {
            Hinzu.ablegen(stplZwischen.inhaltGeben());
            this.ablegen(stplZwischen.entnehmen());
        }   
    }
    
    /**
     * Kopiert diese Stapel, die Elemente sind aber
     * nicht kopiert, nur die Verweise sind dupliziert.
     * @return Verweis auf eine Kopie diese Liste
     */
    public StapelPlus<Inhalt> kopie() {
        StapelPlus<Inhalt> tmpStapel = new StapelPlus<Inhalt>();
        StapelPlus<Inhalt> neueStapel = new StapelPlus<Inhalt>();

        while(!this.istLeer()) {
            tmpStapel.ablegen(this.entnehmen());
        }
        while(!tmpStapel.istLeer()) {
            neueStapel.ablegen(tmpStapel.inhaltGeben());
            this.ablegen(tmpStapel.entnehmen());
        }
        return neueStapel;
    }
 
    /**
     * Die Anzahl der Elemente in der Liste 
     * wird zurückgegeben. 
     * @return Integer - Anzahl von Elemente in der Liste.
     */    
    public Integer laengeGeben() {
        return Laenge;
    }
    
    /**
     * Ein leerer Stapel wird angelegt.
     */
    public StapelPlus() {
        super();
        Laenge = 0;
    }

    /**
     * Löscht die gesamte Stapel.
     */
    public void vernichten() {
        while(!this.istLeer()) {
            this.entnehmen();
        }
    }

}
