/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.rekursion;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_USHORT_GRAY;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import javax.imageio.ImageIO;

/**
 * Eine Hilfs-Klasse für das Damen-problem.
 * @author dominic
 */
public class DamenBrett {
    int n;
    boolean[][] BrettPos;
    /**
     * Erzeugt eine Spielbrett von größe 8 Mal 8 Kacheln.
     */
    public DamenBrett() {
        n=8;
        BrettPos = new boolean[n][n];
        zurruecksetzen();
    }
    
    /**
     * Erzeugt eine Spielbrett von größe k Mal k Kacheln.
     * @param k
     */
    public DamenBrett(int k) {
        n=k;
        BrettPos = new boolean[n][n];
        zurruecksetzen();
    }
    
    /**
     * Gibt die Breite und Höhe von das Spielbrett zurück.
     * @return n
     */
    public int getBreite() {
        return n;
    }
    
    /**
     * Prüft ob der Position von eine Dame besetzt ist oder nicht.
     * Alle Koordinaten können gegeben werden, nut position innerhalb von (0/0) bis(n-1/n-1)
     * können Damen haben.
     * @param a - X-Koorindate der Position, 0 bis n-1
     * @param b - Y-Koorindate der Position, 0 bis n-1
     * @return Wahr: Position von eine Damen betsezt, <br> False: Position nicht von eien Dame betsetzt.
     */
    public boolean getPosition(int a, int b) {
        if (   (a<0) || (a>n-1) 
            || (b<0) || (b>n-1) )
            return false;
        return BrettPos[a][b];
    }
    
    /**
     * Entscheided ob diese Platz frei für eine
     * neue Dame ist, oder nicht.
     * Bei Wahr, kann eine Dame hier platziert werden,
     * bei Flasch sollte keine Dame hier platziert werden.
     * @param a XPos von 0 bis n-1
     * @param b YPos von 0 bis n-1
     * @return Wahr oder Flasch
     */
    public boolean istFrei(int a, int b) {
        for(int i=0; i<n;i++)
        {
            //diagonale überprufen
            if (getPosition(a-i,b-i))
                return false;
            if (getPosition(a+i,b+i))
                return false;
            if (getPosition(a-i,b+i))
                return false;
            if (getPosition(a+i,b-i))
                return false;
            
            // Horizontale und vertikale
            if (getPosition(a+i,b  ))
                return false;
            if (getPosition(a  ,b+i))
                return false;
            if (getPosition(a-i,b  ))
                return false;
            if (getPosition(a  ,b-i))
                return false;
        }
        return true;
    }

    /**
     * Erzeugt eine neue Kopie von genau diese
     * Brett, mit die plattzierte Damen.
     * @return eine Kopie von diese Spielbrett.
     */
    public DamenBrett kopie() {
        DamenBrett Kopie = new DamenBrett(n);
        for(int i=0; i<n;i++)
            for(int j=0; j<n; j++)
                if(BrettPos[i][j])
                    Kopie.setPosition(i, j,true);
        return Kopie;
    }

    /* hilfs Routine, die eine Dame-Bild
    * ladt, um das für das Brett-Bild zu verwenden.
    */
    private BufferedImage ladeDame(int Fliesen) {
        BufferedImage imgDame = null;
        boolean geladen;
        try {
            URL einURL = this.getClass().getResource("resources/dame.png");
            try (InputStream in = einURL.openStream()) {
                imgDame = ImageIO.read(in);
            }
            geladen = true;
        }
        catch (IOException ioe) {
            geladen = false;
        }
        
        if (!geladen) {
            imgDame = new BufferedImage(Fliesen,Fliesen,BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2d = imgDame.createGraphics();
            g2d.setColor(Color.WHITE);
            g2d.fillOval(0, 0, Fliesen, Fliesen);
            g2d.setColor(Color.BLACK);
            g2d.fillOval(Fliesen/4, Fliesen/4
                        ,Fliesen/2, Fliesen/2);
        }
        
        return imgDame;
    }
    
    /**
     * Wert für die Dame auf eine Position auf das Brett setzen.
     * @param a XPos von 0 bis n-1
     * @param b YPos von 0 bis n-1
     * @param bWert: Wahr: Eine Dame ist hier, <br>Falsch: Kein Dame ist hier.
     */
    public void setPosition(int a
                           ,int b
                           ,boolean bWert) {
        if (   (a<0) || (a>n-1) 
            || (b<0) || (b>n-1) )
            return ;
        BrettPos[a][b]=bWert;
    }
    
    /**
     * Gibt eine BufferedImage von der Schachbrett
     * zurück.
     * Bild Quelle für die Dame: http://pixabay.com/de/schach-schachfiguren-schachfigur-316887/#
     * "Creative Commons 0" License am 17.06.2014
     * @param Massen das Bild, falls zu klein ist alles 
     * @return BufferedImage von der Schachbrett mit positionierte Damen.
     **/
    public BufferedImage zuBufferedImage(int Massen) {
        BufferedImage imgDame;
        int Fliesen = Massen/n;
        Massen = Fliesen*n;
        
        BufferedImage Image=new BufferedImage(Massen,Massen,TYPE_USHORT_GRAY);
        
        // zu klein, um etwas zu machen
        if (Massen < 2*n)
            return Image;

        // Bild erstellen, mit weiße Hintergrund
        Graphics2D g2d = Image.createGraphics();
        g2d.setBackground(Color.WHITE);
        g2d.clearRect(0, 0, Massen, Massen);
        
        g2d.setColor(Color.WHITE);
        g2d.setXORMode(Color.BLACK);
        
        // Karierte Hintergrund erstellen.
        for(int i=Fliesen;i<Massen;i+=2*Fliesen) {
            //  fillRect(x, y, breite,  höhe
            g2d.fillRect(i, 0, Fliesen, Massen);
            g2d.fillRect(0, i, Massen,  Fliesen);
        }
        
        //Damenbilder laden und überall hinlegen.
        imgDame = ladeDame(Fliesen);
        int breiteDame = imgDame.getWidth();
        int hoeheDame = imgDame.getHeight();

        // Damen hinlegen
        g2d.setXORMode(Color.BLACK);
        for(int i=0; i<n; i++)
            for(int j=0;j<n;j++)
            {
                if (BrettPos[i][j])
                    g2d.drawImage(imgDame ,i*Fliesen ,j*Fliesen
                                 ,Fliesen ,Fliesen ,null);
            }
        return Image;
    }
    
    private void zurruecksetzen() {
        for(int i=0; i<n;i++)
            for(int j=0; j<n; j++)
                BrettPos[i][j]=false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.n;
        hash = 97 * hash + Arrays.deepHashCode(this.BrettPos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DamenBrett other = (DamenBrett) obj;
        if (this.n != other.n) {
            return false;
        }
        if (!Arrays.deepEquals(this.BrettPos, other.BrettPos)) {
            return false;
        }
        return true;
    }
    
}
