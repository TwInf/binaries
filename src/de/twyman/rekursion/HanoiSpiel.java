package de.twyman.rekursion;
/*
 * Copyright (C) 2014 D. Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import java.util.LinkedList;

/**
 * Eine Hilfsklasse für die Türme von Hanoi
 * @author D.Twyman
 */
public class HanoiSpiel {
    LinkedList<Integer>[] Stangen;
    int iMaxBreite = 1;
    int iMaxHoehe = 1;

    /**
     * Erstelt eine HanoiSpiel mit 3 Stangen,
     * nummiert von 0 bis 2.
     */
    public HanoiSpiel() {
        Stangen = new LinkedList[3];
        for(int i=0; i<Stangen.length;i++)
            Stangen[i] = new LinkedList<Integer>();
    }
    
    public HanoiSpiel(int n) {
        Stangen = new LinkedList[n];
        for(int i=0; i<Stangen.length;i++)
            Stangen[i] = new LinkedList<Integer>();
    }
    
    /**
     * Lege eine Zahl auf Stangenummer s ab.
     * @param s
     * @param iWert 
     */
    public void ablegen(int s,Integer iWert) {
        if ( (s>=Stangen.length) || (s<0) )
            throw new IllegalArgumentException(Integer.toString(s));
        if ( iWert == null )
            throw new IllegalArgumentException("null Parameter");
        if ( iWert <1 )
            throw new IllegalArgumentException("nicht naturliche Parameter");
        if (!Stangen[s].isEmpty()){
            if(Stangen[s].peek().compareTo(iWert)<0)
               throw new IllegalArgumentException(Integer.toString(iWert));
        }
        iMaxBreite=Math.max(iMaxBreite, iWert);
        
        Stangen[s].push(iWert);
        
        int iSummeSize=0;
        for(LinkedList<Integer> Stange:Stangen)
            iSummeSize += Stange.size();
        iMaxHoehe=Math.max(iSummeSize, iMaxHoehe);
    }

    /**
     * Entfernt eine Zahl von eine Stange s.
     * @param s
     * @return das Zahl
     */
    public Integer entnehmen(int s) {
        if ( (s>=Stangen.length) || (s<0) )
            throw new IllegalArgumentException(Integer.toString(s));
        return Stangen[s].pop();
    }
    
    /**
     * Gibt der oberste Wert von Stange s zurück. 
     * @param s
     * @return 
     */
    public Integer inhaltGeben(int s) {
        if ( (s>=Stangen.length) || (s<0) )
            throw new IllegalArgumentException(Integer.toString(s));
        return Stangen[s].peek();
    }
    
    /**
     * Sagt ob Stange s leer ist oder nicht. 
     * @param s
     * @return 
     */
    public boolean istLeer(int s) {
        if ( (s>=Stangen.length) || (s<0) )
            throw new IllegalArgumentException(Integer.toString(s));
        return Stangen[s].isEmpty();
    }

    /**
     * Gibt eine Bild von der Zustand die Stangen zurück. 
     * @param iBreite
     * @param iHoehe
     * @return 
     */
    public BufferedImage zuBufferedImage(int iBreite
                                        ,int iHoehe) {
        BufferedImage Image=new BufferedImage(iBreite,iHoehe,TYPE_INT_RGB );
        Graphics2D g2d = Image.createGraphics();
        g2d.setBackground(Color.WHITE);
        g2d.clearRect(0, 0, iBreite, iHoehe);
        double n = Stangen.length;
        double dSpaltBreite = iBreite/(2*n);
        g2d.setColor(Color.BLUE);
        g2d.setPaint(Color.GRAY);
        g2d.setStroke( new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        for(int i=0;i<n;i++) {
            RoundRectangle2D.Double rctStng = 
                            new RoundRectangle2D.Double(
                               (2*i+1)*dSpaltBreite-5,0
                              ,10,iHoehe
                              ,5,5);
            g2d.fill(rctStng);
        }
        double einheitBreite = dSpaltBreite*2/iMaxBreite;
        double einheitHoehe = ((double)iHoehe)/iMaxHoehe;
        
        int iStng=0;
        for(LinkedList<Integer> Stange:Stangen) {
            for (int iSchiebe=0; iSchiebe<Stange.size(); iSchiebe++) {
                Integer iWert = Stange.get(Stange.size()-1-iSchiebe);
                Color clrSchiebe = new Color(iWert*20%200+50,0,iWert*20%200+50);
                g2d.setPaint(clrSchiebe);
                RoundRectangle2D.Double rctStng = 
                                new RoundRectangle2D.Double(
                                   (2*iStng+1)*dSpaltBreite-(einheitBreite*iWert/2)
                                  ,iHoehe-(iSchiebe+1)*einheitHoehe
                                  ,(einheitBreite*iWert)
                                  ,einheitHoehe
                                  ,5,5);
                g2d.fill(rctStng);
            }
            iStng++;
        }
        
        return Image;
    }
}
