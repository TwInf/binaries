package de.twyman.rekursion;

/*
 * Copyright (C) 2014 D. Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import de.twyman.malen.Punkt;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Eine Hilfs-Klasse für das Springerproblem.
 * @author D. Twyman
 */
public class SpringerBrett {
    int n;
    int[][] BrettPos;
    boolean bZahlenZeigen;
    int Fliesen;
    // Hilfs Klasse für den pfadImageRoutine
    // Bescheunligt die Herstellung von ein Bild
    private class SpringerPunkt extends Punkt {
        private int[][] GueltigeBewegung = {
            {+2,+1},{+2,-1},{-2,+1},{-2,-1}
           ,{+1,+2},{+1,-2},{-1,+2},{-1,-2}};

        public SpringerPunkt(int X
                            ,int Y) {
            super(X,Y);
        }

        private boolean istGueltig(Punkt P1) {
            return (   (P1.getX()>=0) && (P1.getX()<SpringerBrett.this.n) 
                    && (P1.getY()>=0) && (P1.getY()<SpringerBrett.this.n) );
        }

        private boolean istGueltig() {
            return istGueltig(this);
        }

        private ArrayList<SpringerPunkt> getFolgePunkte() {
            ArrayList<SpringerPunkt> P2 = new ArrayList<>();
            for (int[] gb1 : GueltigeBewegung) {
                SpringerPunkt PN = new SpringerPunkt(
                                  this.getX() + gb1[0]
                                 ,this.getY() + gb1[1]);
                if (istGueltig(PN)) {
                    P2.add(PN);
                }
            }
            return P2;
        }
        
        public Line2D zuLine2D(Punkt P2) {
            return new Line2D.Double( this.getX()*Fliesen+Fliesen/2
                                     ,this.getY()*Fliesen+Fliesen/2
                                     ,P2.getX()*Fliesen+Fliesen/2
                                     ,P2.getY()*Fliesen+Fliesen/2);
        }
    }
    
    /**
     * Erzeugt eine Spielbrett von größe 8 Mal 8 Kacheln.
     */
    public SpringerBrett() {
        n=8;
        BrettPos = new int[n][n];
        zurruecksetzen();
        bZahlenZeigen = true;
    }
    
    /**
     * Erzeugt eine Spielbrett von größe k Mal k Kacheln.
     * @param k
     */
    public SpringerBrett(int k) {
        n=k;
        BrettPos = new int[n][n];
        zurruecksetzen();
        bZahlenZeigen = true;
    }
    
    /**
     * Gibt die Breite und Höhe von das Spielbrett zurück.
     * @return n
     */
    public int getBreite() {
        return n;
    }
    
    /**
     * Prüft ob der Position von eine Springer besucht worden ist, oder nicht.
     * @param a - X-Koorindate der Position, 0 bis n-1
     * @param b - Y-Koorindate der Position, 0 bis n-1
     * @return 0: Position nicht besucht worden, <br> n: Schritt Nummer.
     */
    public int getSchritt(int a, int b) {
        if (   (a<0) || (a>n-1) 
            || (b<0) || (b>n-1) )
            return 0;
        return BrettPos[a][b];
    }
   
    private int getSchritt(Punkt P) {
        return getSchritt(P.getX(),P.getY());
    }
   
    /**
     * Erzeugt eine neue Kopie von genau diese
     * Brett, mit die plattzierte Damen.
     * @return eine Kopie von diese Spielbrett.
     */
    public SpringerBrett kopie() {
        SpringerBrett Kopie = new SpringerBrett(n);
        for(int i=0; i<n;i++)
            for(int j=0; j<n; j++)
                Kopie.setSchritt(i, j,BrettPos[i][j]);
        return Kopie;
    }

    /**
     * Schrittnummer für die Springer auf eine Position auf das Brett setzen.
     * @param a XPos von 0 bis n-1
     * @param b YPos von 0 bis n-1
     * @param bWert: Wahr: Eine Dame ist hier, <br>Falsch: Kein Dame ist hier.
     */
    public void setSchritt(int a
                          ,int b
                          ,int bWert) {
        if (   (a<0) || (a>n-1) 
            || (b<0) || (b>n-1) )
            return ;
        BrettPos[a][b]=bWert;
    }
    
    private BufferedImage zahlZuImage(int x) {
        BufferedImage image = new BufferedImage(100,100,TYPE_INT_ARGB);
        Graphics2D g2d = image.createGraphics();
    
        Font f = new Font("SansSerif", Font.PLAIN, 120);
    
        g2d.setFont(f);
        String strZahl = Integer.toString(x);  
        FontRenderContext frc = g2d.getFontRenderContext();
        LineMetrics lm = f.getLineMetrics(strZahl, frc);
        double ascent = lm.getAscent();
        Rectangle2D bounds = f.getStringBounds(strZahl, frc);

        double h = bounds.getHeight();
        double w = bounds.getWidth();
        int Zahlkachel;
        Zahlkachel = (int)Math.max(h, w)+1;
        image = new BufferedImage(Zahlkachel,Zahlkachel,TYPE_INT_ARGB);
        g2d.dispose();
        g2d = image.createGraphics();
        g2d.setFont(f);
        g2d.setColor(Color.WHITE);
        g2d.drawString(strZahl
                      ,(int)((Zahlkachel-w)/2) 
                      ,Zahlkachel-(int)((Zahlkachel-ascent)/2) );
        return image;
    }
    
    /**
     * Gibt eine BufferedImage von der Schachbrett
     * zurück.
     * @param Massen das Bild, falls zu klein ist alles 
     * @return BufferedImage von der Schachbrett mit positionierte Damen.
     **/
    public BufferedImage zuBufferedImage(int Massen) {
        Fliesen = Massen/n;
        Massen = Fliesen*n;
        
        BufferedImage Image=new BufferedImage(Massen,Massen,TYPE_INT_RGB);
        
        // zu klein, um etwas zu machen
        if (Massen < 2*n)
            return Image;

        // Bild erstellen, mit weiße Hintergrund
        Graphics2D g2d = Image.createGraphics();
        g2d.setBackground(Color.WHITE);
        g2d.clearRect(0, 0, Massen, Massen);
        
        g2d.setColor(Color.WHITE);
        g2d.setXORMode(Color.BLACK);
        
        // Karierte Hintergrund erstellen.
        for(int i=Fliesen;i<Massen;i+=2*Fliesen) {
            //  fillRect(x, y, breite,  höhe
            g2d.fillRect(i, 0, Fliesen, Massen);
            g2d.fillRect(0, i, Massen,  Fliesen);
        }
        
        if (this.bZahlenZeigen)
            zahlenImage(Image,g2d);
        else
            pfadImage(Image,g2d);
        return Image;
    }
    
    private void pfadImage(BufferedImage Image
                          ,Graphics2D g2d) {
        g2d.setColor(Color.BLUE);
        g2d.setPaint(Color.BLUE);
        g2d.setPaintMode();
        
        int i=0,j=0;
        // Suche der Anfang.
        sucheAnfag: 
        for(i=0; i<n; i++) {
            for(j=0;j<n;j++) {
                if (BrettPos[i][j]==n*n) {
                   break sucheAnfag;
                }
            }
        }
        int l;
        int k=this.n*this.n;
        g2d.setStroke( new BasicStroke(4f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        SpringerPunkt P = new SpringerPunkt(i,j);
        do {
            l = k;
            for(SpringerPunkt NaechsteP: P.getFolgePunkte()) {
                if (getSchritt(NaechsteP)==(k-1)) {
                    
                    g2d.draw(P.zuLine2D(NaechsteP));
                    // Zeichne Linien
                    P = NaechsteP;
                    k = k-1;
                    break;
                }
            }
        } while (l>k);
        
    }
    
    private void zahlenImage(BufferedImage Image
                            ,Graphics2D g2d) {
        for(int i=0; i<n; i++)
            for(int j=0;j<n;j++)
            {
                if (BrettPos[i][j]>0) {
                    BufferedImage imgZahl = zahlZuImage(BrettPos[i][j]);
                    g2d.drawImage(imgZahl ,i*Fliesen ,j*Fliesen
                                 ,Fliesen ,Fliesen ,null);
                }
            }
    }
    
    private void zurruecksetzen() {
        for(int i=0; i<n;i++)
            for(int j=0; j<n; j++)
                BrettPos[i][j]=0;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + this.n;
        hash = 97 * hash + Arrays.deepHashCode(this.BrettPos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SpringerBrett other= (SpringerBrett) obj;
        if (this.n != other.n) {
            return false;
        }
        if (!Arrays.deepEquals(this.BrettPos, other.BrettPos)) {
            return false;
        }
        return true;
    }

    /**
     * Sagt, ob der Pfad als numerrierte Schritte angezeigt werden soll.
     * Wenn Falsch, dann wird das Pfad als eine Linie gezeichnet.
     * @return Wahr oder Falsch
     */
    public boolean getZahlenZeigen() {
        return bZahlenZeigen;
    }

    /**
     * Setzt, ob der Pfad als nummerierte Schritte angezeigt werden soll.
     * Wenn Falsch, dann wird das Pfad als eine Linie gezeichnet.
     * @param Wahr - Schrittnummern anzeigen, Flasch - Pfad anzeichen
     */
    public void setZahlenZeigen(boolean bZahlenZeigen) {
        this.bZahlenZeigen = bZahlenZeigen;
    }
    
}
